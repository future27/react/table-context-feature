import React, { useState } from "react";
import "./Table.css";
import { BsFillTrashFill, BsFillPencilFill } from "react-icons/bs";
const Table = ({ rows, deleteRow, editRow}) => {

    const [selectedRow, setSelectedRow] = useState([]);
    
    const handleSelectRow = ( event, data) => {
        console.log(event.altKey)
        event.stopPropagation();
        const newRow = {
            description: data.description,
            page: data.page,
        }
        if (event.altKey) {
            event.currentTarget.classList.add("selected__row");
            setSelectedRow([...selectedRow, newRow])
        } else {
            const trs = document.getElementsByTagName("tbody")[0].children;
            for (const el of trs) {
                el.classList.remove("selected__row")
            }
            event.currentTarget.classList.add("selected__row");
            setSelectedRow([newRow])
        }
        
    }

    const eventTargetSelected = () => {
        console.log(selectedRow)
    }

    
  return (
    <div className="table-wrapper">
      <table className="table">
        <thead>
          <tr>
            <th>Page</th>
            <th className="expand">Description</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {rows.map((row, index) => {
            const statusText = row.status.charAt(0) + row.status.slice(1);
            return (
              <tr key={index} onClick={(event) => handleSelectRow(event, {description: row.description, page: row.page})}>
                <td>{row.page}</td>
                <td className="expand">{row.description}</td>
                <td>
                  <span className={`label label-${statusText}`}>
                    {row.status}
                  </span>
                </td>
                <td>
                  <span className="actions">
                    <BsFillTrashFill className="delete-btn" onClick={() => deleteRow(index)}/>
                    <BsFillPencilFill onClick={() => editRow(index)}/>
                  </span>
                </td>
              </tr>
            );
          })}
          
        </tbody>
      </table>
      <button onClick={eventTargetSelected}>Select</button>
    </div>
  );
};

export default Table;
