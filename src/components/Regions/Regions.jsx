import React, {useContext} from "react";
import "./Regions.css";
import {RegionContext} from "../../context/index";
const Regions = () => {
    const {setRegions} = useContext(RegionContext);
  const btns = ["MSK", "PV", "UG", "NW", "SIB"];
  return (
    <div className="reg-btn">
      {btns.map((element, index) => (
        <input
          key={index}
          type="button"
          className="check_init"
          value={element}
          onClick={() => setRegions(element)}
        />
      ))}
    </div>
  );
};

export default Regions;
