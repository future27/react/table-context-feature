export const mskRows = [
    {page: "MSK", description: "A00-server01", status: "live"},
    {page: "MSK", description: "A00-server02", status: "live"},
    {page: "MSK", description: "A00-server03", status: "live"},
    {page: "MSK", description: "A00-server04", status: "live"},
];
export const pvRows = [
    {page: "PV", description: "A04-server01", status: "live"},
    {page: "PV", description: "A04-server02", status: "draft"},
    {page: "PV", description: "A04-server03", status: "live"},
    {page: "PV", description: "A04-server04", status: "error"},
];