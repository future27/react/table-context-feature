import React, {useEffect, useState} from 'react';
import './App.css';
import Modal from './components/Modal/Modal';
import Table from './components/Table/Table';
import Regions from './components/Regions/Regions';
import { RegionContext } from './context';
import {mskRows, pvRows} from "./storage/index";

function App() {
    const [regions, setRegions] = useState("MSK");
    const [modalOpen, setModalOpen] = useState(false);
    const [rows, setRows] = useState(mskRows);
    console.log(`Render with ${regions}`)

    const switchContent = (regions) => {
        console.log(regions)
        if (regions !== "MSK" && regions === "PV") {
            setRows(pvRows);
        } else {
            setRows(mskRows)
        }
    }
    useEffect(() => {
        switchContent(regions);
    }, [regions])
    const [rowEdit, setRowEdit] = useState(null);

    const handleDeleteRow = (targetIndex) => {
        setRows(rows.filter((_, index) => index !== targetIndex))
    }

    const handleEditRow = (index) => {
        setRowEdit(index);

        setModalOpen(true);
    }

    const handleSubmite = (newRow) => {
        rowEdit === null ?
            setRows([...rows, newRow]) : 
                setRows(rows.map((currRow, index) => {
                    if (index !== rowEdit) return currRow;

                    return newRow;
                }))
    }

  return (
    <RegionContext.Provider value={{regions, setRegions}}>
        <div className="App">
        <Regions />
      <Table rows={rows} deleteRow={handleDeleteRow} editRow={handleEditRow}/> 
      <button className="btn" onClick={() => setModalOpen(true)}>
        Add
        </button>
      { modalOpen && <Modal closeModal={() => setModalOpen(false)} onSubmit={handleSubmite} defaultValue={rowEdit !== null && rows[rowEdit]}/>}
    </div>
    </RegionContext.Provider>
  );
}

export default App;
